let Encore = require('@symfony/webpack-encore');
let path = require('path');
require('url-loader');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

Encore
    .cleanupOutputBeforeBuild()
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .addEntry('js/app', './assets/js/app.js')
    //.addEntry('js/script', './assets/js/script.js')
    .addEntry('js/login', './assets/js/login.js')
    .addEntry('js/head', './assets/js/head.js')
    .addEntry('js/index.ie9', './assets/js/index.ie9.js')
    .addEntry('js/index.ie10', './assets/js/index.ie10.js')
    .addEntry('js/main','./assets/js/main.js')
    .addStyleEntry('css/app', './assets/css/app.scss')
    .addStyleEntry('css/login', './assets/css/login.scss')
    .addStyleEntry('css/login1', './assets/css/login1.css')
    .addStyleEntry('css/main' , './assets/css/main.scss')
    // .splitEntryChunks()
    .enableSingleRuntimeChunk()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    // .configureBabel(() => {}, {
    //     useBuiltIns: 'usage',
    //     corejs: 3
    // })
    .autoProvidejQuery()
    .autoProvideVariables({
        'window.jQuery': 'jquery',
        'window.$': 'jquery'
    })
    .enableSassLoader(function(sassOptions) {}, {
        resolveUrlLoader: false
    })
    .enableVueLoader(function(options) {


        postLoaders:{
            html: 'babel-loader'

        }
    })
    .copyFiles({
        from: './assets/images',
        to: 'images/[path][name].[ext]',
    })

    .devServer = {
    host: 'localhost',
    port: 8015,
    contentBase: ('/public/'),
    publicPath: 'localhost:8031/build/',
    watchContentBase: false,
    compress: true,
    open: true,
    disableHostCheck:true,
    hot: true,
    progress:true,
}
;
let backendConfig = Encore.getWebpackConfig();
backendConfig.name = 'backend';

backendConfig.resolve.alias = {
    Base: path.resolve(__dirname, 'assets/vendor/h2impression/Base.js'),
    Component: path.resolve(__dirname, 'assets/vendor/h2impression/Component.js'),
    Config: path.resolve(__dirname, 'assets/vendor/h2impression/Config.js'),
    Plugin: path.resolve(__dirname, 'assets/vendor/h2impression/Plugin.js'),
    Menubar: path.resolve(__dirname, 'assets/vendor/h2impression/Section/Menubar.js'),
    GridMenu: path.resolve(__dirname, 'assets/vendor/h2impression/Section/GridMenu.js'),
    Sidebar: path.resolve(__dirname, 'assets/vendor/h2impression/Section/Sidebar.js'),
    PageAside: path.resolve(__dirname, 'assets/vendor/h2impression/Section/PageAside.js'),
    AppCalendar: path.resolve(__dirname, 'assets/vendor/h2impression/Calendar.js'),
    Site: path.resolve(__dirname, 'assets/vendor/h2impression/Site.js'),
    BaseApp: path.resolve(__dirname, 'assets/vendor/h2impression/BaseApp.js'),
    Skycons: path.resolve(__dirname, 'assets/vendor/skycons/skycons.js'),
    asScrollbar: path.resolve(__dirname, 'node_modules/jquery-asScrollbar/dist/jquery-asScrollbar.es.js/jquery-asScrollbar'),
    asScrollable: path.resolve(__dirname, 'node_modules/jquery-asScrollable/dist/jquery-asScrollable.es.js'),
    Switchery: path.resolve(__dirname, 'node_modules/switchery-npm/index.js'),
    tokenfield: path.resolve(__dirname, 'node_modules/bootstrap-tokenfield/dist/bootstrap-tokenfield.js'),
    DataTable: path.resolve(__dirname, 'node_modules/datatables.net/js/jquery.dataTables.js'),
    SignaturePad: path.resolve(__dirname, 'node_modules/signature_pad/dist/signature_pad.js')
};

module.exports = [backendConfig];